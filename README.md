# Test discovery et gateway avec SpringBoot

Le but est de tester la mise en place de la discovery et de la gateway via SpringBoot ainsi que de mettre en place 
un environement résilient.

+ Définition de 2 zones de service
+ Chaqu'une des zones contient :
    + 1 discovery
    + plusieurs instance d'un service
+ Le régles de disponibilité des service sont : 
    + les services de la __zone 1__ devront prioritairement être accessible via la discovery de la __zone 1__
    + les services de la __zone 2__ devront prioritairement être accessible via la discovery de la __zone 2__
    + les services présents dans la __zone 1__ devront être connus par la discovery de la __zone 2__
    + les services présents dans la __zone 2__ devront être connus par la discovery de la __zone 1__
    + si la discovery de la __zone 1__ tombe les services de la zone 1 devront être disponible via la discovery de la __zone 2__
    + si la discovery de la __zone 2__ tombe les services de la zone 2 devront être disponible via la discovery de la __zone 1__

```
                                   +---------+
                                   | gateway |
                                   +---------+
                                        |
                   +-----------------------------------------+
                   |                                         |
+------------------|------------------+   +------------------|------------------+
| ZONE1            |                  |   | ZONE2            |                  |
|                  |                  |   |                  |                  |
|            +-----------+            |   |            +-----------+            |
|            | discovery |===========synchro===========| discovery |            |
|            +-----------+            |   |            +-----------+            |
|                  |                  |   |                  |                  |
|         +--------+--------+         |   |         +--------+--------+         |
|         |                 |         |   |         |                 |         |
| +---------------+ +---------------+ |   | +---------------+ +---------------+ |
| |  service 1-A  | |  service 1-B  | |   | |  service 2-A  | |  service 2-B  | |
| +---------------+ +---------------+ |   | +---------------+ +---------------+ |
+-------------------------------------+   +-------------------------------------+
```

1. Création d'un service
2. Mise en place des discovery : Eureka
   1. Définition de zones
   2. Enregistrement des services
2. Mise en place de la gateway : Spring Gateway
   1. Connexion aux discovery
   2. Tests
3. Customisation des hearthbeat
4. Customisation du processus de self-preservation
(https://dzone.com/articles/the-mystery-of-eurekas-self-preservation)
    
##  Création d'un service

##  Mise en place des discovery

###  Définition de zones

### Enregistrement des services

##  Mise en place de la gateway

### Connexion aux discovery

### Tests

## Customisation des hearthbeat

## Customisation du processus de self-preservation